'use strict';

/* Controllers */

var bisineerControllers = angular.module('bisineer.controllers', []);

/*var registeringCustomer = {};*/

/*bisineerControllers.controller('RegisterCtrl', [
	'$scope', 
	'LabelFactory',
	'OrganizationFactory', 
	'UserFactory',
	'RegisterCustomerFactory', 
	'$location', 
	function($scope, LabelFactory, OrganizationFactory, UserFactory, RegisterCustomerFactory, $location) {
		console.log("inside register controller");

		// initialize labels
		$scope.pageDetails = {};
		$scope.pageDetails.pageName = 'register';
		$scope.pageDetails.locale = 'en';
		console.log("page details are " + JSON.stringify($scope.pageDetails));
		LabelFactory.getLabels($scope.pageDetails, function(data){
			console.log("data returned is " + JSON.stringify(data));
			console.log("heading is " + data.labels.heading);
			$scope.labels = data.labels;
		});

		// populate the data on register form on clicking back button
		$scope.customer = RegisterCustomerFactory;

		$scope.accessToOtherSystems = function(access){
			$scope.customer.hasAccessToOtherSystems = access;
			if(!$scope.customer.hasAccessToOtherSystems){
				$scope.customer.accessSystems = [];
			}
		}

		$scope.requestUserAccess = function(){
			console.log("RegisterCtrl.requestUserAccess");
			console.log("customer data " + JSON.stringify($scope.customer));
			// add customer.status = PTA (pending territory manager approval)
			UserFactory.requstUserAccess($scope.customer, function(data){
				console.log("data returned by api call is " + JSON.stringify(data));
				if(data.result === "success"){
					$location.path('/register-success');
				}else{
					$scope.error = data.errorMsg;
				}
			});
			
		}
	}
]);*/

bisineerControllers.controller('LoginCtrl', [
	'$scope','LabelFactory',
	function($scope, LabelFactory){
		console.log('inside LoginCtrl');
		LabelFactory.getLabels($scope.pageDetails, function(data){
			console.log("data returned is " + JSON.stringify(data));
			console.log("heading is " + data.labels.heading);
			$scope.labels = data.labels;
		});
	}]
);

bisineerControllers.controller('ResetPasswordCtrl', [
	'$scope','LabelFactory',
	function($scope, LabelFactory){
		console.log('inside LoginCtrl');
		LabelFactory.getLabels($scope.pageDetails, function(data){
			$scope.labels = data.labels;
		});
	}]
);

bisineerControllers.controller('ProfileCtrl', [
	'$scope','LabelFactory',
	function($scope, LabelFactory){
		console.log('inside ProfileCtrl');
		LabelFactory.getLabels($scope.pageDetails, function(data){
			$scope.labels = data.labels;
		});
	}]
);

bisineerControllers.controller('OrderHistoryCtrl', [
	'$scope','LabelFactory',
	function($scope, LabelFactory){
		console.log('inside OrderHistoryCtrl');
		LabelFactory.getLabels($scope.pageDetails, function(data){
			$scope.labels = data.labels;
		});
	}]
);

bisineerControllers.controller('ProductDetailsCtrl', [
	'$scope','LabelFactory','ProductDetailsFactory',
	function($scope, LabelFactory, ProductDetailsFactory){
		console.log('inside ProductDetailsCtrl');
		LabelFactory.getLabels($scope.pageDetails, function(data){
			$scope.labels = data.labels;
		});
		ProductDetailsFactory.getProductDetails($scope.product, function(data){
			$scope.product = data.product;
			/*This block is to set the tabContent when page loads.*/
			console.log("product attributes returned is " + JSON.stringify($scope.product.attributes));
			for (var i=0; i < $scope.product.attributes.length; i++){
				var attribute = $scope.product.attributes[i];
				if(attribute.isSelected.localeCompare("active") === 0){
					$scope.tabContent = attribute.description;
					break;
				}
			}
		});
		$scope.toggleImage = function(imgUrl){
			$scope.full_image_url = imgUrl;	
		}
		$scope.full_image_url = "images/sanitary_hot_water/1.jpg";
		$scope.toggleTabContent = function(attribute){
			$scope.tabContent = attribute.description;
		}
	}
]);

bisineerControllers.controller('PumpsCtrl', [
	'$scope','LabelFactory','ProductFactory',
	function($scope, LabelFactory, ProductFactory){
		console.log('inside PumpsCtrl');
		
		LabelFactory.getLabels($scope.pageDetails, function(data){
			$scope.labels = data.labels;
		});

		ProductFactory.getProducts($scope.category, function(data){
			$scope.products = data.products;
		});
	}]
);

bisineerControllers.controller('DashboardCtrl', ['$scope','LabelFactory',
	function($scope, LabelFactory){
		console.log("inside DashboardCtrl");
	}]
);

