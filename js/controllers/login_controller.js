'use strict';

bisineerControllers.LoginCtrl = function($scope, $location, $rootScope, $timeout, LabelFactory, UserService, CartService){
	console.log('inside LoginCtrl');
	LabelFactory.getLabels($scope.pageDetails, function(data){
		console.log("data returned is " + JSON.stringify(data));
		console.log("heading is " + data.labels.heading);
		$scope.labels = data.labels;
	});

	$scope.login = function(user){
		UserService.login.query(user, function(data){
			$location.path('/category/Pumps');
			//$rootScope.user = data;
			// changed from user to globalUser. because user is used inside many other 
			//controllers and if you use user at global level, then all those inside 
			//controllers will be overridden with global one. Dang!! disaster!!
			$rootScope.globalUser = data; 
			UserService.user = data;
			updateCart();
			$rootScope.alertMessage = "Welcome " + UserService.user.first_name + " !!";
			$rootScope.showAlert = true;
			$rootScope.alertClass = 'alert alert-dismissable alert-success';
			$timeout($rootScope.hideAlertBox, 4000);
		}, function(error){
			$location.path('/login');
			$rootScope.alertMessage = error.data;
			$rootScope.showAlert = true;
			$rootScope.alertClass = 'alert alert-dismissable alert-danger';
		});
	}

	// update cart for user
	var updateCart = function(){
		var clientCart = CartService.getCart();
		if(!angular.isUndefined(clientCart.line_items) && clientCart.line_items.length > 0){
			CartService.flushCartOnServer.query(clientCart, 
				function(data){
					if(!angular.isUndefined(data.line_items)){
						CartService.setLineItems(data.line_items);	
					}
				}, function(error){
					// ignore
					//though there is a mismatch b/w client cart and server cart, at this point ignore
					console.log("couldn't flush cart on server");
				}
			);
		}else{
			CartService.getCartFromServer.query(function(data){
				var serverCart;
				serverCart = data;
				if (!angular.isUndefined(serverCart.line_items) && serverCart.line_items.length > 0){
					CartService.setCart(serverCart);
					for(var i=0; i<serverCart.line_items.length ; i++){
						var line_item = serverCart.line_items[i];
						$rootScope.totalQuantity += line_item.quantity;
					}
				}
			}, function(error){
				// ignore
				//though there is a mismatch b/w client cart and server cart, at this point ignore
				console.log("couldn't get cart from server");
			});
		}
	}
	// toggle error message alert box
	$rootScope.hideAlertBox = function(){
		if($rootScope.showAlert){
			$rootScope.showAlert = !$rootScope.showAlert;	
		}
	}
}