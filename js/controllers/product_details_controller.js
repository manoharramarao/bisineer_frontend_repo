'use strict';

bisineerControllers.ProductDetailsCtrl = function($scope, LabelFactory, ProductDetailsFactory){
	console.log('inside ProductDetailsCtrl');
	LabelFactory.getLabels($scope.pageDetails, function(data){
		$scope.labels = data.labels;
	});
	ProductDetailsFactory.getProductDetails($scope.product, function(data){
		$scope.product = data.product;
		/*This block is to set the tabContent when page loads.*/
		console.log("product attributes returned is " + JSON.stringify($scope.product.attributes));
		for (var i=0; i < $scope.product.attributes.length; i++){
			var attribute = $scope.product.attributes[i];
			if(attribute.isSelected.localeCompare("active") === 0){
				$scope.tabContent = attribute.description;
				break;
			}
		}
	});
	$scope.toggleImage = function(imgUrl){
		$scope.full_image_url = imgUrl;	
	}
	$scope.full_image_url = "images/sanitary_hot_water/1.jpg";
	$scope.toggleTabContent = function(attribute){
		$scope.tabContent = attribute.description;
	}
}