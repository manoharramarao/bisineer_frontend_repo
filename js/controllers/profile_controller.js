'use strict';

bisineerControllers.ProfileCtrl = function($scope, LabelFactory, UserService){
	console.log('inside ProfileCtrl');
	LabelFactory.getLabels($scope.pageDetails, function(data){
		$scope.labels = data.labels;
	});

	if(angular.isUndefined(UserService.user) || UserService.user === null){
		UserService.getUser.query(function(data){
			if(data.result === "failure"){
				$rootScope.alertMessage = data.msg;
				$rootScope.showAlert = true;
				$rootScope.alertClass = 'alert alert-dismissable alert-danger';
			}else{
				UserService.setUser(data);
				$scope.user = data; // you need to do this here, because these are async calls.
			}
		});
	}else{
		$scope.user = UserService.user;			
	}
};