'use strict';

bisineerControllers.SideCartCtrl = function($scope, $rootScope, CartService){
	console.log('inside SideCartCtrl');
	
	$scope.expandMinicart = function(){
		console.log("inside expandMinicart");
	}


	// move this to global controller. Get rid of cart controller. All $rootscope values on page refresh should come from global controller
	CartService.getCartFromServer.query(function(data){
		var cart;
		$rootScope.totalQuantity = 0;
		cart = data;
		if (!angular.isUndefined(cart.line_items) && cart.line_items.length > 0){
			var i;
			for(i = 0; i < cart.line_items.length; i++){
				$rootScope.totalQuantity += cart.line_items[i].quantity;
			}
		}
		CartService.setCart(cart);
		console.log(JSON.stringify(CartService.getCart()));
	});
}
