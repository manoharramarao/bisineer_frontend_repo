'use strict';

// this is the controller that would be executed on page refresh
bisineerControllers.GlobalCtrl = function($scope, $rootScope, $location, $timeout, CartService, UserService, CategoryService){
	console.log('inside GlobalCtrl');
	
	// get user name from server on page refrest based on session
	UserService.getUser.query(function(data){
		if(data.result === "failure"){
			$rootScope.alertMessage = data.msg;
			$rootScope.showAlert = true;
			$rootScope.alertClass = 'alert alert-dismissable alert-danger';
		}else{
			UserService.setUser(data);
			$rootScope.globalUser = data; // you need to do this here, because these are async calls.
		}
	});

	CartService.getCartFromServer.query(function(data){
		var cart;
		$rootScope.totalQuantity = 0;
		cart = data;
		if (!angular.isUndefined(cart.line_items) && cart.line_items.length > 0){
			var i;
			for(i = 0; i < cart.line_items.length; i++){
				$rootScope.totalQuantity += cart.line_items[i].quantity;
			}
			console.log("cart inside globalcontroller " + JSON.stringify(cart));
			CartService.setCart(cart);
		}
		console.log(JSON.stringify(CartService.getCart()));
	});

	CategoryService.getCategories.query(function(data){
		if(data.result === "failure"){
			/*do nothing*/
		}else{
			data.categories.sort();
			CategoryService.categories = data.categories;
			console.log(JSON.stringify(CategoryService.categories));
			$scope.categories = {};
			$scope.categories.few = [];
			$scope.categories.more = [];
			for(var i=0; i<3; i++){
				$scope.categories.few[i] = CategoryService.categories[i];
			}
			for(var i=3; i < CategoryService.categories.length; i++){
				$scope.categories.more[i-3] = CategoryService.categories[i];
			}
		}
	});

	$scope.onCartClick = function(){
		$location.path('/cart');
		/*UserService.isUserLoggedin.query(function(data){
			UserService.setUser(data);
			$location.path('/cart');
		}, function(error){
			$location.path('/login');
		});*/
	}

	$scope.logout = function(){
		
		// clearing cart
		CartService.setCart({});
		$rootScope.totalQuantity = 0;
		
		console.log("logging out user " + JSON.stringify(UserService.user));
		UserService.logout.query(UserService.user, function(data){ // no need to pass UserService.user here.
			if(data.result === "failure"){
				$rootScope.alertMessage = data.msg;
				$rootScope.showAlert = true;
				$rootScope.alertClass = 'alert alert-dismissable alert-danger';
				$rootScope.globalUser = {}
				UserService.user = {}
			}else{
				UserService.user = {};
				$location.path('/login');
				$rootScope.globalUser = {}
				UserService.user = {}
				$rootScope.alertMessage = data.msg
				$rootScope.showAlert = true;
				$rootScope.alertClass = 'alert alert-dismissable alert-success';
				$timeout($rootScope.toggleAlertBox, 4000);
			}
		});
	}

	// toggle error message alert box
	$rootScope.toggleAlertBox = function(){
		$rootScope.showAlert = !$rootScope.showAlert;
	}
}
