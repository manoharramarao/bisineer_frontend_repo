'use strict';

bisineerControllers.LogoutCtrl = function($scope, $location, $rootScope, $timeout, UserService, CartService){
	console.log('inside LogoutCtrl');
	
	$scope.logout = function(){
		
		// clearing cart
		var cart = {};
		cart.line_items = [];
		CartService.setCart(cart);
		$rootScope.totalQuantity = 0;
		
		console.log("logging out user " + JSON.stringify(UserService.user));
		UserService.logout.query(UserService.user, function(data){ // no need to pass UserService.user here.
			if(data.result === "failure"){
				$rootScope.alertMessage = data.msg;
				$rootScope.showAlert = true;
				$rootScope.alertClass = 'alert alert-dismissable alert-danger';
				$rootScope.globalUser = {}
				UserService.user = {}
			}else{
				UserService.user = {};
				$location.path('/login');
				$rootScope.globalUser = {}
				UserService.user = {}
				$rootScope.alertMessage = data.msg
				$rootScope.showAlert = true;
				$rootScope.alertClass = 'alert alert-dismissable alert-success';
				$timeout($rootScope.toggleAlertBox, 4000);
			}
		});
	}

	// toggle error message alert box
	$rootScope.toggleAlertBox = function(){
		$rootScope.showAlert = !$rootScope.showAlert;
	}
}