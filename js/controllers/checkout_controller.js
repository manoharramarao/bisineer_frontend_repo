'use strict';

bisineerControllers.CheckoutCtrl = function($scope, $location, LabelFactory, AddressService, CartService){
	console.log('inside CheckoutCtrl');
	
	LabelFactory.getLabels($scope.pageDetails, function(data){
		console.log("data returned is " + JSON.stringify(data));
		console.log("heading is " + data.labels.heading);
		$scope.labels = data.labels;
	});

	$scope.cart = CartService.getCart();
	console.log("cart when entering checkout page " + JSON.stringify($scope.cart));

	$scope.showKeepShoppingButton = true;
	$scope.showProceedToCheckoutButton = false;
	$scope.showSubmitOrderButton = true;
	
	AddressService.getAddresses.query(
		function(data){
			$scope.addresses = data;
		},
		function(error){
			console.log(JSON.stringify(error));
		}
	);

	$scope.open = function() {
		$scope.showModal = true;
	};
	$scope.ok = function() {
		$scope.showModal = false;
	};
	$scope.cancel = function() {
		$scope.showModal = false;
	};

	$scope.save_address = function(address){
		console.log("clicked save_address button");
		console.log("address sent to server is " + JSON.stringify(address));
		address.address_type="shipping"
		AddressService.saveAddress.query(address, 
			function(data){
				console.log("after returning to client, data is " + JSON.stringify(data));
				$scope.addresses[data['id']] = data;
				console.log("list of addresses is " + JSON.stringify($scope.addresses));
				$scope.showModal = false;
			},
			function(error){
				console.log("error occurred");
				console.log(error.data);
			}
		);
	}
}