'use strict';

/* Controllers */

/*var pumpsController = angular.module('bisineer.controllers', []);*/

/*bisineer.controller('PumpsCtrl', [
	'$scope','LabelFactory','ProductFactory',
	function($scope, LabelFactory, ProductFactory){
		console.log('inside PumpsCtrl');
		
		LabelFactory.getLabels($scope.pageDetails, function(data){
			$scope.labels = data.labels;
		});

		ProductFactory.getProducts($scope.category, function(data){
			$scope.products = data.products;
		});
	}
]);*/

bisineerControllers.ProductCtrl = function($scope, $rootScope, $routeParams, $timeout, LabelFactory, ProductService, CartService){
	console.log('inside ProductCtrl');
	console.log('category = ' + $routeParams.categoryName)
	var categoryDetails = {};
	categoryDetails = {"categoryName": $routeParams.categoryName};

	LabelFactory.getLabels($scope.pageDetails, function(data){
		$scope.labels = data.labels;
	});

	ProductService.getProducts.query(categoryDetails, function(data){
		console.log("received data is " + JSON.stringify(data))
		ProductService.products = data.products;
		$scope.products = ProductService.products;
	});

	$scope.addToCart = function(product){
		var cart = {}
		console.log("inside addToCart. Product is " + JSON.stringify(product));
		cart = CartService.addToCart(product);
		if(!angular.isUndefined(cart.line_items) && cart.line_items.length > 0){
			$rootScope.alertMessage = "Item successfully added to cart";
			$rootScope.showAlert = true;
			$rootScope.alertClass = 'alert alert-dismissable alert-success';
			$timeout($rootScope.toggleAlertBox, 2000);
		}

		CartService.flushCartOnServer.query(cart, function(data){
			console.log(JSON.stringify(data.line_items));
			if(!angular.isUndefined(data.line_items)){
				CartService.setLineItems(data.line_items);	
			}
			console.log(CartService.getCart());
		}, function(error){
			console.log("couldn't flush cart on server");
		});
		$rootScope.totalQuantity = CartService.getTotalQuantity();
	}

};