'use strict';

bisineerControllers.RegisterCtrl = function($scope, LabelFactory, UserService, $timeout, $location, $rootScope) {
	console.log("inside RegisterCtrl");

	// initialize labels
	$scope.pageDetails = {};
	$scope.pageDetails.pageName = 'register';
	$scope.pageDetails.locale = 'en';
	console.log("page details are " + JSON.stringify($scope.pageDetails));
	LabelFactory.getLabels($scope.pageDetails, function(data){
		$scope.labels = data.labels;
	});

	// register user
	$scope.registerUser = function(user){
		console.log("inside registerController.registerUser");
		UserService.register.query(user, function(data){
			/*console.log("data returned by api call is " + JSON.stringify(data));*/
			$rootScope.user = {}
			UserService.user = {}
			console.log(JSON.stringify(data));
			$location.path('/login');
			$rootScope.alertMessage = data.msg;
			$rootScope.showAlert = true;
			$rootScope.alertClass = 'alert alert-dismissable alert-success';
			$timeout($rootScope.hideAlertBox, 4000);
		}, function(error){
			console.log("inside error");
			console.log(JSON.stringify(error));
			$rootScope.alertMessage = error.data;
			$rootScope.showAlert = true;
			$rootScope.alertClass = 'alert alert-dismissable alert-danger';
		});
	}

	// toggle error message alert box
	$rootScope.hideAlertBox = function(){
		if($rootScope.showAlert){
			$rootScope.showAlert = !$rootScope.showAlert;	
		}
	}
};