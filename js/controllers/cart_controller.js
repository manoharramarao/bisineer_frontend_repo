'use strict';

bisineerControllers.CartCtrl = function($scope, $location, LabelFactory, CartService){
	console.log('inside CartCtrl');
	
	LabelFactory.getLabels($scope.pageDetails, function(data){
		console.log("data returned is " + JSON.stringify(data));
		console.log("heading is " + data.labels.heading);
		$scope.labels = data.labels;
	});

	$scope.showKeepShoppingButton = false;
	$scope.showProceedToCheckoutButton = true;
	$scope.showSubmitOrderButton = true;

	console.log(JSON.stringify(CartService.getCart()));
	$scope.cart = CartService.getCart();

	CartService.getPrices.query(CartService.getCart(), function(data){
		console.log("prices returned are " + JSON.stringify(data));
		$scope.cart = data;
		CartService.setCart(data);
	}, function(error){
		// ignore. 
		console.log("couldn't get prices");
	});

	$scope.onClickProceedToCheckout = function(){
		$location.path("/checkout");
	}
}