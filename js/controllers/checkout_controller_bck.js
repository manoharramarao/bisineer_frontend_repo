'use strict';

bisineerControllers.CheckoutCtrl = function($scope, $location, LabelFactory, AddressService){
	console.log('inside CheckoutCtrl');
	
	LabelFactory.getLabels($scope.pageDetails, function(data){
		console.log("data returned is " + JSON.stringify(data));
		console.log("heading is " + data.labels.heading);
		$scope.labels = data.labels;
	});

	AddressService.getAddresses.query(
		function(data){
			console.log("shipping address returned is " + JSON.stringify(data));
		},
		function(error){
			console.log(JSON.stringify(error));
		}
	);

	$scope.open = function() {
		$scope.showModal = true;
	};
	$scope.ok = function() {
		$scope.showModal = false;
	};
	$scope.cancel = function() {
		$scope.showModal = false;
	};

	$scope.save_address = function(address){
		console.log("clicked save_address button");
		console.log("address sent to server is " + JSON.stringify(address));
		address.type="shipping"
		AddressService.saveAddress.query(address, 
			function(data){
				console.log("after returning to client, data is " + JSON.stringify(result));
			},
			function(error){
				console.log("error occurred");
				console.log(error.data);
			}
		);
	}
}