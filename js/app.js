'use strict';

/* App Module */

var bisineer = angular.module('bisineer', [
  'ngRoute',
  'ngAnimate',
  'ngSanitize',
  /*'angular-carousel',*/
  'checklist-model',
  /*'bisineer.controllers',*/
  'ui.bootstrap.modal',
  'bisineer.services',
]);

var bisineerControllers = {}
/*bisineerControllers = angular.module('bisineer.controllers', []);*/
bisineer.controller(bisineerControllers);

bisineer.config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
    $routeProvider.
      when('/register', {
        templateUrl: 'partials/register.html',
        controller: 'RegisterCtrl'
      }).
      when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'LoginCtrl'
      }).
      when('/dashboard',{
        templateUrl: 'partials/dashboard.html',
        controller: 'DashboardCtrl'
      }).
      /*when('/category/:categoryName',{
        templateUrl: 'partials/pumps.html',
        controller: 'PumpsCtrl'
      }).*/
      /*when('/category/:categoryName',{
        templateUrl: 'partials/plp.html',
        controller: 'ProductCtrl'
      }).*/
      when('/category/:categoryName', {
        templateUrl: 'partials/category.html',
        controller: 'CategoryCtrl'
      }).
      when('/reset_password', {
        templateUrl: 'partials/reset_password.html',
        controller: 'ResetPasswordCtrl'
      }).
      when('/profile',{
        templateUrl: 'partials/profile.html',
        controller: 'ProfileCtrl'
      }).
      when('/product_details',{
        templateUrl: 'partials/product_details.html',
        controller: 'ProductDetailsCtrl'
      }).
      when('/order_history', {
        templateUrl: 'partials/order_history.html',
        controller: 'OrderHistoryCtrl'
      }).
      /*when('/orderreview', {
        templateUrl: 'partials/orderreview.html',
        controller: 'OrderReviewCtrl'
      }).*/
      when('/cart', {
        templateUrl: 'partials/cart.html',
        controller: 'CartCtrl'
      }).
      when('/checkout', {
        templateUrl: 'partials/checkout.html',
        controller: 'CheckoutCtrl'
      }).
      when('/sidecart', {
        templateUrl: 'templates/sidecart.html',
        controller: 'SideCartCtrl'
      }).
      when('/home', {
        templateUrl: 'partials/home.html',
        controller: 'HomeCtrl'
      }).
      otherwise({
        redirectTo: '/home'
      });

    /*$locationProvider.html5Mode(true);*/
  }
]);

/*bisineer.config(['$httpProvider', 
  function($httpProvider){
    console.log('before deleting X-Requested-With header');
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.headers.post['X-PINGOTHER'] = 'pingpong';
  }
])*/

$httpProvider.interceptors.push(function($q, $rootScope) {
  return {
     'request': function(config) {
        numLoadings++;
        console.log("started http request");
        $rootScope.alertMessage = "Loading..."
        $rootScope.showAlert = true;
        $rootScope.alertClass = 'alert alert-dismissable alert-danger';
        return config || $q.when(config)
     },
     'response': function(response) {
        console.log("finished http request");
        $rootScope.toggleAlertBox()
        return response || $q.when(response);
     },
     'responseError': function(response) {
        console.log("finished http request");
        $rootScope.toggleAlertBox()
        return $q.reject(response);
     }
  };
}).config(function ($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
});




// toggle error message alert box
$rootScope.toggleAlertBox = function(){
  $rootScope.showAlert = !$rootScope.showAlert;
}