'use strict';

var services = angular.module('bisineer.services', ['ngResource']);

/*var baseUrl = 'http://localhost\\:8000/learningweb2py';*/
var baseUrl = "http://localhost:8080";
/*var baseUrl = "http://beta-store.appspot.com";*/

var registeringUser = {};

services.factory('LabelFactory', ['$resource', function($resource){
	console.log("inside LabelFactory");
	return $resource(baseUrl + '/data/labels/labels_register.data', {}, {
		getLabels: {
			method: 'GET',
			isArray: false,
			cache: true
		}
	})
}]);

services.factory("UserService", ['$resource', function($resource){
	var user = {};
	return {
		register: $resource(baseUrl + '/betastore/user/register.json', {}, {
			query: {
				method: 'POST',
				params: {},
				headers: {'Content-Type': 'application/json; charset=UTF-8'},
				isArray: false
			}
		}),
		// gets user details from server session
		getUser: $resource(baseUrl + '/betastore/user/get_user.json', {}, {
			query: {
				method: 'POST',
				params: {},
				headers: {'Content-Type': 'application/json'},
				isArray: false
			}
		}),
		setUser: function(user){
			this.user = user;
		},
		login: $resource(baseUrl + '/betastore/user/login.json', {}, {
			query: {
				method: 'POST',
				params: {},
				headers: {'Content-Type': 'application/json; charset=UTF-8'},
				isArray: false
			}
		}),
		logout: $resource(baseUrl + '/betastore/user/logout.json', {}, {
			query: {
				method: 'POST',
				params: {},
				isArray: false
			}
		}),
		isUserLoggedin: $resource(baseUrl + '/betastore/user/is_logged_in.json', {}, {
			query: {
				method: 'POST',
				params: {},
				isArray: false
			}
		})
	};
}]);

services.factory("ProductService", ['$resource', function($resource){
	var products = {};
	return {
		getProducts: $resource(baseUrl + '/betastore/bisineer_products/get_products.json', {}, {
			query: {
				method: 'POST',
				params: {},
				headers: {'Content-Type': 'application/json'},
				isArray: false // check this. I guess it should be true here.
			}
		})
	};
}]);

services.factory("CategoryService", ['$resource', function($resource){
	var products = {};
	return {
		getCategories: $resource(baseUrl + '/betastore/bisineer_categories/get_categories.json', {}, {
			query: {
				method: 'POST',
				params: {},
				headers: {'Content-Type': 'application/json'},
				isArray: false // check this. I guess it should be true here.
			}
		}),
		getChildCategories: $resource(baseUrl + '/betastore/bisineer_categories/get_child_categories', {}, {
			query: {
				method: "POST",
				params: {},
				headers: {'Content-Type': 'application/json'},
				isArray: true
			}
		})
	};
}]);

services.factory("AddressService", ['$resource', function($resource){
	var addresses = [];
	return {
		saveAddress: $resource(baseUrl + '/betastore/address/save_address.json', {}, {
			query: {
				method: 'POST',
				params: {},
				headers: {'Content-Type': 'application/json'},
				isArray: false // check this. I guess it should be true here.
			}
		}),
		getAddresses: $resource(baseUrl + '/betastore/address/get_addresses.json', {}, {
			query: {
				method: 'POST',
				params: {},
				headers: {'Content-Type': 'application/json'},
				isArray: false // check this. I guess it should be true here.
			}
		})
	}
}]);

services.factory("CartService", ['$resource', function($resource){
	var cart = {};
	cart.line_items = [];
	function getCart(){
		return cart;
	}

	function setCart(new_cart){
		console.log("inside setCart");
		console.log(JSON.stringify(new_cart));
		cart = new_cart;
		return cart;
	}

	function setLineItems(line_items){
		cart.line_items = line_items;
	}

	function getTotalQuantity(){
		var totalQuantity = 0;
		if (cart.line_items.length > 0){
			var i;
			for(i = 0; i < cart.line_items.length; i++){
				totalQuantity += cart.line_items[i].quantity;
			}
		}
		return totalQuantity;
	}

	function addToCart(product){
		var modified_line_items = []
		console.log(JSON.stringify(cart));
		var isAdded = false;
		if(!angular.isUndefined(cart.line_items) && cart.line_items.length > 0){
			var i;
			for (i=0; i < cart.line_items.length; i++){
				var existing_line_item;
				existing_line_item = cart.line_items[i];
				console.log(JSON.stringify(existing_line_item));
				console.log("new product id is " + product.id);
				if (existing_line_item.product_code === product.code){
					existing_line_item.quantity++;
					isAdded = true;
				}
				modified_line_items.push(existing_line_item);
			}	
		}
		if(!isAdded){
			var line_item = {};
			line_item.product_code = product.code;
			line_item.name = product.name;
			line_item.description_short = product.description_short;
			line_item.quantity = 1;
			modified_line_items.push(line_item);
		}
		cart.line_items = modified_line_items
		console.log(JSON.stringify(cart));
		return cart;
	}

	return {
		flushCartOnServer: $resource(baseUrl + '/betastore/cart/flush_cart.json', {}, {
			query: {
				method: 'POST',
				params: {},
				headers: {'Content-Type': 'application/json'},
				isArray: false // check this. I guess it should be true here.
			}
		}),
		getCartFromServer: $resource(baseUrl + '/betastore/cart/get_cart.json', {}, {
			query: {
				method: 'POST',
				params: {},
				headers: {'Content-Type': 'application/json'},
				isArray: false // check this. I guess it should be true here.
			}
		}),
		getPrices: $resource(baseUrl + '/betastore/cart/get_prices.json', {}, {
			query: {
				method: 'POST',
				params: {},
				headers: {'Content-Type': 'application/json'},
				isArray: false // check this. I guess it should be true here.
			}
		}),
		addToCart: addToCart,
		getCart: getCart,
		setCart: setCart,
		getTotalQuantity: getTotalQuantity,
		setLineItems: setLineItems
	}
}]);

/*services.factory("AlertService", function(){
	var alert = {};
	// toggle alert box
	var toggleAlertBox = function(){
		this.alert.showAlert = !this.alert.showAlert
	}
});*/

// this is not working as of now, so not used
services.factory('AlertService', function(){
	var alert = {};

	var toggleAlertBox = function(){
		this.alert.showAlert = !this.alert.showAlert
	}
	return {}
});

/*services.factory('ProductFactory', ['$resource',function($resource){
	console.log("inside ProductFactory");
	return $resource(baseUrl + '/data/products.data', {}, { // ideally, you would be posting category ID and get products belong to that category
		getProducts: {
			method: 'POST',
			isArray: false,
			cache: true
		}
	})
}]);*/

services.factory('ProductDetailsFactory', ['$resource',function($resource){
	console.log("inside ProductDetailsFactory");
	return $resource(baseUrl + '/data/product_details.data', {}, { // ideally, you would be posting category ID and get products belong to that category
		getProductDetails: {
			method: 'POST',
			isArray: false,
			cache: true
		}
	})
}]);
